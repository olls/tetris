Tetris
======

A simple Python terminal implementation of a random Tetris game. This is dependent on my terminal [graphics](https://github.com/olls/graphics) module. Beat my high-score of 222 :)

- `console.py`: Finds the width and height of the terminal, might not work in all environments, came from StackOverflow.
- `letters.py`: A module to convert a string to large ASCII art style text.


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/olls/tetris/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

